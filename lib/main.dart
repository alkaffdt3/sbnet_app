import 'package:flutter/material.dart';
import 'package:sbnet_app/pages/home.dart';
import 'package:sbnet_app/pages/loginPage.dart';
import 'package:sbnet_app/pages/transfer.dart';
import 'package:sbnet_app/style.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: WarnaPrimary
      ),
      debugShowCheckedModeBanner: false,
      home: Home(),
    );
  }
}