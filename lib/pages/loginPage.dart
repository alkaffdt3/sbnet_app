import 'package:flutter/material.dart';
import 'package:sbnet_app/style.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: WarnaPrimary,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "SBNet",
              style: TextStyle(color: Colors.white, fontSize: 40),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              width: 293,
              child: TextField(
                decoration: InputDecoration(
                  border: border,
                  focusedBorder: borderFocused,
                  fillColor: Colors.white,
                  hintText: 'No. HP',
                  filled: true,
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              width: 293,
              child: TextField(
                obscureText: true,
                decoration: InputDecoration(
                  border: border,
                  focusedBorder: borderFocused,
                  fillColor: Colors.white,
                  hintText: 'Password',
                  filled: true,
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            SizedBox(
              height: 35,
            ),
            SizedBox(
              width: 142,
              height: 41,
              child: FlatButton(
                child: Text(
                  "Masuk",
                  style: TextStyle(color: WarnaPrimary),
                ),
                color: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: BorderSide(color: Colors.white)),
                onPressed: () {},
              ),
            ),
            SizedBox(height: 15),
            Text("Lupa kata sandi?", style: TextStyle(
              color: Colors.white,
              fontSize: 15
            ),)
          ],
        ),
      ),
    );
  }
}
