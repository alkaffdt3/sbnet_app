import 'package:flutter/material.dart';
import 'package:sbnet_app/style.dart';

class TransferPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Transfer", style: TextStyle(color: Colors.white)),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              color: Colors.white,
              icon: const Icon(
                Icons.arrow_back,
              ),
              onPressed: () {
                Navigator.pop(
                  context,
                  MaterialPageRoute(builder: (context) => TransferPage()),
                );
              },
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            );
          },
        ),
      ),
      body: Container(
          margin: EdgeInsets.fromLTRB(28, 20, 27, 0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text("Saldo",
                        style: TextStyle(fontSize: 13, color: WarnaPrimary)),
                  ),
                  Align(
                    child: Text("Rp. 5.000.000",
                        style: TextStyle(
                          fontSize: 13,
                        )),
                  )
                ],
              ),
              SizedBox(
                height: 35.17,
              ),
              TextFormField(
                decoration: InputDecoration(
                    labelText: "Nama Tujuan",
                    enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: WarnaPrimary, width: 3.0))),
              ),
              SizedBox(
                height: 25.17,
              ),
              TextFormField(
                decoration: InputDecoration(
                    labelText: "Nominal",
                    enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: WarnaPrimary, width: 3.0))),
              ),
              SizedBox(
                height: 25.17,
              ),
              TextFormField(
                decoration: InputDecoration(
                    labelText: "Catatan",
                    enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: WarnaPrimary, width: 3.0))),
              ),
              SizedBox(
                height: 45,
              ),
              SizedBox(
                  width: 142,
                  height: 41,
                  child: FlatButton(
                    color: Colors.grey,
                    child: Text(
                      "Lanjut",
                      style: TextStyle(color: Colors.white),
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0)),
                    onPressed: () {},
                  )),
            ],
          )),
    );
  }
}
