import 'package:flutter/material.dart';
import 'package:sbnet_app/pages/tagihan.dart';
import 'package:sbnet_app/pages/transfer.dart';
import 'package:sbnet_app/style.dart';
import 'package:sbnet_app/widgets/navbar.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Navbar(),
      body: Container(
        margin: EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Selamat Datang, ",
                    style: TextStyle(
                        color: WarnaPrimary, fontWeight: FontWeight.normal),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Text(
                      "Fulan",
                      style: TextStyle(
                          color: WarnaPrimary,
                          fontSize: 20,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 19),
              child: Container(
                width: 301,
                height: 57,
                decoration: BoxDecoration(
                    color: WarnaPrimary,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.all(Radius.circular(11))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset('assets/images/saldo.png'),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 7),
                          child: Text(
                            'Saldo',
                            style: TextStyle(fontSize: 13, color: Colors.white),
                          ),
                        ),
                        Row(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(bottom: 9),
                              child: Text(
                                'Rp  ',
                                style: TextStyle(
                                    fontSize: 10, color: Colors.white),
                              ),
                            ),
                            Text(
                              '1.000.000',
                              style:
                                  TextStyle(fontSize: 20, color: Colors.white),
                            ),
                          ],
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            IconContainer()
          ],
        ),
      ),
    );
  }
}

class IconContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      // margin: EdgeInsets.all(34),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: GestureDetector(
              child: Column(
                children: <Widget>[
                  Image.asset('assets/images/transfer.png'),
                  SizedBox(
                    height: 4,
                  ),
                  Text(
                    'Transfer',
                    style: TextStyle(fontSize: 10),
                  )
                ],
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => TransferPage()),
                );
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: GestureDetector(
              child: Column(
                children: <Widget>[
                  Image.asset('assets/images/tagihan.png'),
                  SizedBox(
                    height: 4,
                  ),
                  Text(
                    'Tagihan',
                    style: TextStyle(fontSize: 10),
                  )
                ],
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => TagihanPage()),
                );
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: GestureDetector(
              child: Column(
                children: <Widget>[
                  Image.asset('assets/images/pembayaran.png'),
                  SizedBox(
                    height: 4,
                  ),
                  Text(
                    'Transfer',
                    style: TextStyle(fontSize: 10),
                  )
                ],
              ),
              onTap: () {},
            ),
          )
        ],
      ),
    );
  }
}
