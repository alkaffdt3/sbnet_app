import 'package:flutter/material.dart';

class Navbar extends StatefulWidget {
  @override
  _NavbarState createState() => _NavbarState();
}

class _NavbarState extends State<Navbar> {
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: 0,
      selectedItemColor: Colors.lightBlue,
      
      items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.home, color: Colors.grey,),
          title: Text("Beranda", style: TextStyle(color: Colors.grey, fontSize: 10),)
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.contacts, color: Colors.grey,),
          title: Text("Kontak", style: TextStyle(color: Colors.grey, fontSize: 10),)
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.compare_arrows, color: Colors.grey,),
          title: Text("Transaksi", style: TextStyle(color: Colors.grey, fontSize: 10),)
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.mail, color: Colors.grey,),
          title: Text("Inbox", style: TextStyle(color: Colors.grey, fontSize: 10),)
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person, color: Colors.grey,),
          title: Text("Profil", style: TextStyle(color: Colors.grey, fontSize: 10),)
        ),
      ],
    );
  }
}

