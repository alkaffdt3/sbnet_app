import 'package:flutter/material.dart';

//_____________________________________________
//_______________Color Library_________________
//_____________________________________________
const WarnaPrimary = const Color(0xFF02EE7D);

//_____________________________________________
//_______________Border Style_________________
//_____________________________________________
final border = OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(11.0)),
    borderSide: BorderSide.none);

final borderFocused = OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(11.0)),
    borderSide: BorderSide(color: Colors.green));
